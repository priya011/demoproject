package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.example.demo.Entity.Department;

public interface Deptserviceinterface {

	List<Department> getdept();

	Department adddept(Department dep);

	void deletedepartment(Integer dept_id);

	void findbydeptid(Integer deptid);

	//Optional<Department> finddept(Integer id);

	
	
}
