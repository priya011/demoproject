package com.example.demo.service;

import java.util.List;


import com.example.demo.Entity.Salary;

public interface saleryinterface {

//getting the list of salary	
	List<Salary> getsalary();
//getting the details of salary by id
	Salary newsalary(Salary sal);
//deleting the details of salary
	void deletedsalary(Integer sal_id);
//updating the details of salary
	void updatedsalary(Salary sal);

}
