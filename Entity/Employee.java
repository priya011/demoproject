package com.example.demo.Entity;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//import org.springframework.boot.context.properties.bind.Name;
import org.springframework.http.HttpStatus;


@Entity
@Table(name="employee_info")
 
//@OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
public class Employee  {
	
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY )
@Column(updatable = false, nullable = false)
private Integer id;

@NotEmpty
@Size(min = 4, message = "user name should have at least 4 characters")
private String name;

@Min(value = 1, message = "Enter valid age")
private Integer age;


@Pattern(regexp="(^$|[0-9]{10})", message="Enter valid mobile number")
@NotEmpty(message="Enter your mobile number")
private String mobile_num;
@NotBlank(message="Enter your email id")
@Email(message="Email should be valid")
private String email_id;
@NotEmpty(message="Blood group Cannot be empty")
private String blood_group;
public Employee(Integer id, String name, int age, String mobile_num, String email_id, String blood_group) {
	
	this.id = id;
	this.name = name;
	this.age = age;
	this.mobile_num =  mobile_num;
	this.email_id = email_id;
	this.blood_group = blood_group;
}




public Employee() {
	
}

public Integer getId() {
	return id;
}


public void setId(Integer id) {
	this.id = id;
}


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Integer getAge() {
	return age;
}
public void setAge(Integer age) {
	this.age = age;
}
public String getMobile_num() {
	return mobile_num;
}
public void setMobile_num(String mobile_num) {
	this.mobile_num = mobile_num;
}
public String getEmail_id() {
	return email_id;
}
public void setEmail_id(String email_id) {
	this.email_id = email_id;
}
public String getBlood_group() {
	return blood_group;
}
public void setBlood_group(String blood_group) {
	this.blood_group = blood_group;
}
@Override
public String toString() {
	return "employee [id=" + id + ", name=" + name + ", age=" + age + ", mobile_num=" + mobile_num + ", email_id="
			+ email_id + ", blood_group=" + blood_group + "]";
}


}